Web application presenting my educations, my professional experiences, the personal and professional projects that I have made as well as the skills that I have been able to acquire.

1. This web application is broken down into 2 parts which are:
    1. 1st part: backend in Python with the Django framework for the base of the backend and the Django Rest framework to build my API
    2. 2nd part: frontend in Javascript with the Nuxt framework which will allow you to make API calls to create, read, update and delete  data.

2. Technical characteristics:
    1. Programming language (s): Javascript, Python
    2. Framework (s): Bulma, Django, Django-Rest, Nuxt
